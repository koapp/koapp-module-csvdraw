(function() {
  'use strict';

  angular
    .module('csvdraw', [])
    .config(['$compileProvider', function ($compileProvider) {
      $compileProvider.aHrefSanitizationWhitelist(/^\s*.*:/);
    }])
    .controller('csvdrawController', loadFunction);

  loadFunction.$inject = ['$scope', 'structureService', '$location', '$rootScope', '$http', '$window'];

  function loadFunction($scope, structureService, $location, $rootScope, $http, $window) {
    //Register upper level modules
    structureService.registerModule($location, $scope, 'csvdraw');

    // --- Start apiconectionController content ---
    structureService.launchSpinner('.holds-the-iframe');
    $rootScope.isBusy  = true;
   
    var config = $scope.csvdraw.modulescope;
    //let moduleIndex = structureService.getIndex().split("/").slice(-1)[0];

    $scope.config = config;

    if(config.csvData === "" && config.urlData === "") errorManager("Missing Data");
    //config.page = "Hoja 1";
    const apiId =  config.googleApiKey; //"AIzaSyBAGbtyetQ_MneMt25uEptQQQWiDtDF4PM";
    
    //get the csv from the config or from a url and call the init function
    if(config.csvData && config.urlData.trim().length < 1){
      init(config.csvData);
    }else if(localStorage.getItem("csvLoginData") && localStorage.getItem("csvLoginData") != "logedOut"){
        manageSheetRequests(config.page, JSON.parse(localStorage.getItem("csvLoginData")), init);
         config.useLogin = false;
    }else{
       manageSheetRequests(config.page,false,  init);
    }
    
    //define visualization type
    $scope.type = config.type;
    $scope.errorMessage = false; 
    $scope.itemDetail = false;
    $scope.detailData;
    $scope.embedModule = `${config.embedModule}?backUrl=${$location.url()} &&url=`;
    $scope.searchitem = $location.search().q;
    //console.log($location.url());
    
   //transform the csv to a json and create a scope objet ready to be used by the view
    function init(data, isJson){
      let json = [];
      if(!isJson){
        json = CSVJSON.csv2json(data, {parseNumbers: true});
      }else{
        json = data;
      }
      
      $scope.allData = json;
      
      $scope.data = json;
      
      //create internal variables
      let listConfig = {
        field1: {
            csvVar: config.staticsFields.field1.csvVar,
            type: config.staticsFields.field1.type
        },
        field2: {
            csvVar: config.staticsFields.field2.csvVar,
            type: config.staticsFields.field2.type
        },
        field3: {
            csvVar: config.staticsFields.field3.csvVar,
            type: config.staticsFields.field3.type
        }
      }; 
      
      //get user specifications to add custom csv vars to the view
      config.fieldsConfig.forEach((element, index)=>{
        listConfig["customVar" + index] = {
            csvVar: element.csvName,
            type: element.fieldType,
            style: stringToStyleObject(element.style)
        }
      });
      
      function stringToStyleObject(styles){
          let result = {};
          if(!styles || styles === "") return;
          styles.split(";").forEach((style)=>{
              if(!style) return;
              let prop = style.split(":");
              result[prop[0]] = prop[1];
          });
          return result
      }
      
      //enable the relationship list of vars in the view
      $scope.fieldsNames = listConfig;
      //console.log("relation betwen csv vars and wiew vars", listConfig);
      
      
      $scope.elements = createListElements(listConfig, json);
      //console.log("final elements", $scope.elements);
      

      function createListElements(config, data){
          let listElements = [];
          let index = 0;
          data.forEach(element => {
              let newElement = {}
              Object.keys(config).forEach(itemName => {
                newElement.id = index;
                if(config[itemName].csvVar){
                    newElement[itemName] = element[config[itemName].csvVar];
                }else{
                    newElement[itemName] = element[config[itemName]];
                }
              })
              listElements.push(newElement);
              index++;
          })
          return listElements;
      }
    }
    
    
    //manage sheet requests
    function manageSheetRequests(page, filter, callback){
        console.log("manage requests", page, filter );
        let parsedData = [];
        
        //get the sheet id from the url
        let sheetId= config.urlData.split("/");
        sheetId = sheetId[sheetId.findIndex((element) => element === "d" )+1];
      
        //fix sheet page
        if(!config.page){errorManager("Missing page in the config"); return}
        page = encodeURIComponent(page);
      
        const url = `https://sheets.googleapis.com/v4/spreadsheets/${sheetId}/values/${page}?majorDimension=ROWS&&key=${apiId}`;
        //build the request
        var req = {
            method: 'GET',
            url: url
        }
        $http(req).then((result) => parseData(result));
        
        function parseData(result){
            let items = result.data.values;
            for(let i = 1; i < items.length; i++){
                parsedData.push({});
                
                for(let y = 0; y < items[0].length; y++){
                    let index = items[0][y];
                    let value = items[i][y];
                    parsedData[i-1][index] = value;
                    
                }
            }
            
            if(filter){
                
                let index = parsedData.length - 1;
                while (index >= 0) {
                  if(parsedData[index][filter.field] && parsedData[index][filter.field] != filter.userValue ){
                    parsedData.splice(index, 1);
                  }
                
                  index -= 1;
                }
            }
            callback(parsedData, true);
        }
    }

    //function to manage the type of size
    $scope.fieldSize = function(size){
      if(config.type === "list"){
        return {"max-width":  size + '%'}
      }else{
        return {"max-height": size + '%' }
      }
    }

    $scope.openFullScrean = function(itemUrl){
      if(itemUrl === "prop") return;
      localStorage.setItem("csvPath", itemUrl);
      localStorage.setItem("returnUrl", "#"+ $location.$$path );
      $window.location.href = "#" + config.childrenUrl.csvembed;
    }
    
    let storeType;
    $scope.goToDetail = function(id){
        let item = $scope.elements.find(element => element.id === id);
        
        $scope.detailHead = {
            "name": item.name,
            "image": item.image,
            "description": item.description
        }
        let itemCopy = Object.assign({}, item);
        delete itemCopy.id;
        delete itemCopy.name;
        delete itemCopy.image;
        delete itemCopy.description;
        
        $scope.detailBody = itemCopy;
        
        
        //console.log($scope.detailHead, $scope.detailBody);
        storeType = $scope.type;
        $scope.type = "detail"
        
        //console.log("Go to detail", id);
    }
    
    $scope.backToList = function(){
        $scope.type = storeType;
    }
    
    function errorManager(errMsg, errorLog){
        console.error(errMsg);
        $scope.errorMessage = errMsg? errMsg:false
        if(errorLog) console.error(errorLog)
    }

    $rootScope.isBusy = false;

  }
}());
